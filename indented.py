#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-mapletax-timber@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import inspect
import textwrap
import contextlib
import functools


logger = logging.getLogger(__name__)


def _find_stack_depth():
	frame = inspect.currentframe()
	depth = 0
	depth_logging = 0
	while frame:
		if frame.f_code.co_filename == logging.__file__:
			depth_logging = depth
		depth += 1
		frame = frame.f_back
	return depth - depth_logging


def _format_indented(old_format, base_level, record):
	res = old_format(record)
	stack_depth = _find_stack_depth()
	indent = ' ' * (stack_depth - base_level)
	return textwrap.indent(res, indent)


@contextlib.contextmanager
def monkeypatch_stack_indenting_formatter(base_level=5):
	"""
	Monkey-patch logging formatter to add indent prefix based on stack depth
	"""
	old_formats = dict()
	for handler in logging.root.handlers:
		old_formats[handler] = handler.formatter.format
		handler.formatter.format = functools.partial(
		 _format_indented, old_formats[handler], base_level)
	yield
	for handler, old_format in old_formats.items():
		handler.formatter.format = old_format

