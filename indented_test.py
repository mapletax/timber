import logging

from . import indented


logger = logging.getLogger(__name__)


def test_monkeypatch_stack_indenting_formatter():
	with indented.monkeypatch_stack_indenting_formatter(15):
		logger.info("hello")
		def test():
			logger.info("world")
		test()

