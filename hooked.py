#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-mapletax-timber@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import contextlib


@contextlib.contextmanager
def monkeypatch_hook_method_after(obj, method, func):
	"""
	Wrap an object's `method` so as to call a function after it's called
	"""
	old_method = getattr(obj, method)

	if isinstance(obj, logging.Logger):
		"""
		logging keeps track of the stack, so we need special treatment here.
		"""
		logger = obj
		level = getattr(logging, method.upper())

		def new_method(msg, *args, **kw):
			kw.setdefault("stacklevel", kw.get("stacklevel", 1)+1)

			if logger.isEnabledFor(level):
				ret = logger._log(level, msg, args, **kw)
			else:
				ret = None

			func(msg, *args, **kw)
			return ret

	else:
		def new_method(*args, **kw):
			ret = old_method(*args, **kw)
			func(*args, **kw)
			return ret

	setattr(obj, method, new_method)
	yield
	setattr(obj, method, old_method)

